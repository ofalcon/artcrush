import React, { Fragment } from 'react';
import './Home.css';
import imagen1 from './../../img/Post1.jpg';
import imagen2 from './../../img/Post2.jpg';
import imagen3 from './../../img/Post3.jpg';
import {
    Link
  } from "react-router-dom";

const Home = () => {
    return ( 
        <Fragment>
            <div className="container">
                <header> <h1 className="home-header">
               - Lo último en posts! - </h1>
                </header>
               
                <div className="row row-col-3">
                    <div className="col">
                    <Link to="/Post1">
                    <div className="card bg-dark text-white home-card">
                        <img src={imagen1} className="card-img" alt="..."/>
                             <div className="card-img-overlay">
                              <h5 className="card-title">Reparar pared dañada</h5>
                                  <p className="card-text"> Es muy sencillo y se consigue en tan solo unos pasos. ¡En este tutorial te lo demostramos!</p>
                                  <p className="card-text">By Lucy_lu91</p>
                                    </div>
                    </div>
                    </Link>
                   
                    </div>
                    <div className="col">
                        <Link to="/Post2">
                        <div className="card bg-dark text-white home-card">
                        <img src={imagen2} className="card-img" alt="..."/>
                             <div className="card-img-overlay">
                              <h5 className="card-title">Mis nuevas ilustraciones en acuarelas</h5>
                                  <p className="card-text">He estado experimentando con esta fascinante técnica y te muestro mis resultados</p>
                                  <p className="card-text">By Sia_liner</p>
                                    </div>
                    </div>
                        </Link>
                 
                    </div>
                    <div className="col">
                        <Link to="/Post3">
                        <div className="card bg-dark text-white home-card">
                        <img src={imagen3} className="card-img" alt="..."/>
                             <div className="card-img-overlay">
                              <h5 className="card-title">Crean mural con materiales reciclados</h5>
                                  <p className="card-text">Te contamos de esta innovacion que se dio en Caracas.</p>
                                  <p className="card-text">By TwistedSoul92</p>
                                    </div>
                    </div>
                        </Link>
                
                    </div>
                </div>
            </div>
        </Fragment>
     );
}
 
export default Home;