import React from 'react';
import './About.css';


const About = () => {
    return ( 
        <div className="container">
            <div className="row">
                <div className="col col-4">
                    <div className="quienes-somos">
                        <p id="que-es">¿Qué es ArtCrush?</p>
                        <p id="somos">Somos una comunidad libre que involucra todo lo que encierra el arte</p>
                        <p id="animate">Animate a explorar nuestro contenido</p>
                    </div>
                </div>
                <div className="col col-8 about-2">
                    <div className="descubri-posts">
                        <p id="descubri"> Descubrí contenido</p>
                        <p>Sumergite en nuestra creatividad colectiva</p>
                    </div>

                    <div className="hace-posts">
                        <p id="crea">O crea tu propio post</p>
                        <p>Pon tu grano de arena en este crecimiento dionisíaco</p>
                    </div>

                </div>
            </div>
        </div>
     );
}
 
export default About;