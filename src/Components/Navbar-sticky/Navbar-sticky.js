import React from 'react';
import './Navbar-sticky.css';
import logo from './../../img/Logo.png';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const NavBarSticky = () => {
    return ( 
        <nav class="navbar navbar-expand-lg sticky-top">
        <Link to="/Home" class="navbar-brand" >
            <img src={logo} width="55px"></img>ArtCrush</Link>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav  nav-container">
            <li class="nav-item active">
              <Link to="/Home" class="nav-link" href="#">|   Inicio   |<span class="sr-only">(current)</span></Link>
            </li>
            <li class="nav-item">
              <Link to="/Explore" class="nav-link" href="#" data-target="#exampleModal">|   Posts   |</Link>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">|
                Categorias   |
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Exposiciones</a>
                <a class="dropdown-item" href="#">Manualidades</a>
                <a class="dropdown-item" href="#">Noticias</a>
              </div>
            </li>
            <li class="nav-item">
            </li>
          </ul>
          
        </div>
      </nav>
     );
}
 
export default NavBarSticky;