import React from 'react';
import logo from './../../img/Logo.png'
import './MainLogo.css'

const MainLogo = () => {
    return ( 
        <div className="container">
            <div className="row main-row">
                <div className="col-4">
                    <img className="main-image" src={logo} alt="" width="300px"/>
                </div>
                <div className="col-8 main-start">
                    <h1 id="titulo">ArtCrush!</h1>
                    <h4 id="slogan">La colisión que necesitas!</h4>
                </div>
            </div>
        </div>
     );
}
 
export default MainLogo;