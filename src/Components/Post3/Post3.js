import React, {Fragment} from 'react';
import imagen1 from './../../img/Post3.jpg';
import './Post3.css'


const Post3 = ({titulo, poster, fecha}) => {
    return ( <Fragment>

 <div className="container">
     <div className="row">

         <div className="col col-lg-8">
             <div className="Encabezado">
             <h1 className="mt-4">{titulo}</h1>
    <p class="lead">
          por
          <a href="#">{poster} </a>
        </p>

        <hr/>

    <p>{fecha}</p>
             </div>
<hr/>

<img class="img-fluid rounded" src={imagen1} alt=""/>

<hr/>

        <p className="lead Encabezado">Miles de tapas de plástico, que podrían haber terminado en la basura, se transformaron en un colorido mural en Caracas, Venezuela. El artista Óscar Olivares dijo que su idea era “hacer algo que realmente impactara a las personas”. MIRA: Artista brasileño pinta mural con lodo para recordar a las víctimas del colapso de la represa en Brumadinho</p>

       <hr/>
       <img src={imagen1} className="post3-img" alt=""/>

        <div class="card my-4">
          <h5 class="card-header">Leave a Comment:</h5>
          <div class="card-body">
            <form>
              <div class="form-group">
                <textarea class="form-control" rows="3"></textarea>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>

      
        <div class="media mb-4">
          <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt=""/>
          <div class="media-body">
            <h5 class="mt-0">Commenter Name</h5>
            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
          </div>
        </div>

     
        <div class="media mb-4">
          <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt=""/>
          <div class="media-body">
            <h5 class="mt-0">Commenter Name</h5>
            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.

            <div class="media mt-4">
              <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt=""/>
              <div class="media-body">
                <h5 class="mt-0">Commenter Name</h5>
                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
              </div>
            </div>

            <div class="media mt-4">
              <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt=""/>
              <div class="media-body">
                <h5 class="mt-0">Commenter Name</h5>
                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
              </div>
            </div>

          </div>
        </div>

      </div>
         </div>
     </div>
 


    </Fragment>
       

     );
}
 
export default Post3;