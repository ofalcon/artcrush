import React from 'react';
import './Explore.css';
import Home from './../Home/Home'



const Explore = () => {

   

    return ( 
        <div className="container explorador">
           <h1 className="titulo-explorer">Explorá nuestro contenido por categoría</h1>


           <select className="form-control form-control-lg" id="select-categoria">
           <option disabled selected value="chaschas">Selecciona una categoría</option>
           <option value="Manualidades">Manualidades</option>
           <option value="Exposicion">Exposición</option>
           <option value="Noticias" data-taget="#exampleModal">Noticias</option>

           </select>
           <hr/>
           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
          Explorar
        </button>

            <div class="modal fade myModal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
              <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Por ahora, estos son los posts disponibles de esa categoria:</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <Home/>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Okis</button>
                <button type="button" class="btn btn-primary">Chauchis</button>
              </div>
            </div>
          </div>
        </div>

        </div>
     );
}
 
export default Explore;