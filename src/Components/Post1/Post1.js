import React, {Fragment} from 'react';
import imagen1 from './../../img/Post1.jpg';
import './Post1.css'


const Post1 = ({titulo, poster, fecha}) => {
    return ( <Fragment>

 <div className="container">
     <div className="row">

         <div className="col col-lg-8">
             <div className="Encabezado">
             <h1 className="mt-4">{titulo}</h1>
    <p class="lead">
          por
          <a href="#">{poster} </a>
        </p>

        <hr/>


    <p>{fecha}</p>


             </div>
    
<hr/>


<img class="img-fluid rounded" src={imagen1} alt=""/>

<hr/>

        <p className="lead Encabezado">¿Tienes paredes de gotelé? A simple vista, pueden parecer más difíciles de reparar que las paredes lisas. Sin embargo, devolver todo su esplendor al gotelé es muy sencillo y se consigue en tan solo unos pasos. ¡En este tutorial te lo demostramos!</p>


        <blockquote class="blockquote">
          <p class="mb-0">HERRAMIENTAS Espátula Lija Rodillo</p>
          <footer class="blockquote-footer Encabezado">MATERIALES Masilla reparadora Pintura Spray para reparar gotelé
           
          </footer>
        </blockquote>

        <p>1. RASCA Y LIJA LAS ZONAS DAÑADAS
¡Manos a la obra! Antes de nada, como vas a estar utilizando spray y pintura más tarde, asegúrate de cubrir bien con papel o plástico todas las zonas que no quieres que se manchen como el suelo, el techo o los muebles cercanos. Ponte también unos guantes para protegerte las manos.

A continuación, si tu pared tiene algún tipo de material adherido como en el caso de la imagen, utiliza una espátula para ir sacando poco a poco las manchas. Una vez hayas eliminado la mayor parte, toma una lija de mano y pásala por toda la superficie, alisándola lo más que puedas.</p>

        <p>2. TAPA LOS AGUJEROS EN TU PARED DE GOTELÉ CON MASILLA
El siguiente paso es tapar con un poco de masilla agujeros, desconchones y otros desperfectos que tenga tu pared. Puedes utilizar un masilla en polvo, que tendrás que preparar antes mezclándo con agua, o una masilla reparadora que ya venga lista para usar. En cualquier caso, aplícala con una espátula, asegurándote de que la capa de producto quede lo más uniforme posible.

Tras la aplicación, deja secar el tiempo que indique el fabricante. Si tu masilla no merma, una capa de producto puede ser suficiente. Si merma tras el secado, lija ligeramente y vuelve a aplicar una capa con la espátula, dejando secar después.</p>

        <p>3. LIJA LA MASILLA
Una vez la masilla esté completamente seca, vuelve a lijar la superficie reparada. Después, coge un plumero y pásalo por la zona, eliminando cualquier resto de polvo que se haya podido generar durante el proceso.</p>

        <p>4. REPARAR EL GOTELÉ CON UN SPRAY
Es momento de aplicar el reparador de gotelé en spray en las zonas afectadas. No es nada fácil obtener esta textura de forma rápida sin realizar la técnica completa en la pared, pero un spray especial con efecto gotelé hará el trabajo mucho más sencillo.

Agita bien el bote antes de usar el producto y, después, aplícalo como lo harías con una pintura en spray: siempre en la misma dirección. Evita mantener el spray mucho tiempo en un mismo lugar, ya que de lo contrario el producto no quedará uniforme.

Una vez que hayas cubierto toda la superficie, deja secar el tiempo recomendado por el fabricante.</p>

<p>5. PINTA LA PARED
Ya solo te queda dar el toque final a la pared, pintándola. Escoge la misma pintura que ya tenías aplicada o, si lo prefieres, puedes escoger una de otro color o brillo, para darle un toque completamente renovado a la habitación. Utiliza un rodillo y presta especial atención a las zonas reparadas para que queden totalmente camufladas. Después, deja que la pintura seque.</p>

        <hr/>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/pk_mzP21W0o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

      
        <div class="card my-4">
          <h5 class="card-header">Leave a Comment:</h5>
          <div class="card-body">
            <form>
              <div class="form-group">
                <textarea class="form-control" rows="3"></textarea>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>

      
        <div class="media mb-4">
          <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt=""/>
          <div class="media-body">
            <h5 class="mt-0">Commenter Name</h5>
            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
          </div>
        </div>

     
        <div class="media mb-4">
          <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt=""/>
          <div class="media-body">
            <h5 class="mt-0">Commenter Name</h5>
            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.

            <div class="media mt-4">
              <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt=""/>
              <div class="media-body">
                <h5 class="mt-0">Commenter Name</h5>
                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
              </div>
            </div>

            <div class="media mt-4">
              <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt=""/>
              <div class="media-body">
                <h5 class="mt-0">Commenter Name</h5>
                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
              </div>
            </div>

          </div>
        </div>

      </div>
         </div>
     </div>
 


    </Fragment>
       

     );
}
 
export default Post1;