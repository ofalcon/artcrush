import React from 'react';
import './App.css';
import NavBarSticky from './Components/Navbar-sticky/Navbar-sticky';
import MainLogo from './Components/MainLogo/MainLogo';
import Home from './Components/Home/Home';
import About from './Components/About/About';
import Footer from './Components/Footer/Footer';
import Explore from './Components/Explore/Explore';
import Post1 from './Components/Post1/Post1';
import Post2 from './Components/Post2/Post2';
import Post3 from './Components/Post3/Post3';
import Modal from './Components/Modal/Modal';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  var f = new Date();
var fecha=(f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear());

  return (
//PLanificacion:
    //Primero estructura fija: navbar sticky y navbar relative. //Asides que simulen publicidad.

    //luego, home page
    //Luego paginas de post
    //Ubicar donde CARAJO meter el modal


    <div className="App">
      <Router>
        <NavBarSticky/>
        <Switch>
          <Route path="/Home">
           <MainLogo/>
           <Home/>
            <About/>
          </Route>

          <Route path="/Explore">
            <Explore/>
          </Route>

          <Route path="/Post1">
            <Post1
              titulo="Repara tu pared dañada"
              poster=" Lucylu_91"
              fecha={fecha}/>
          </Route>

          <Route path="/Post2">
          <Post2
              titulo="Mis nuevas ilustraciones en Acuarela"
              poster=" Sia_Liner"
              fecha={fecha}/>
          </Route>

          <Route path="/Post3">
            <Post3
              titulo="Crean mural con materiales reciclados"
              poster="TwistedSoul92"
              fecha={fecha}/>
          </Route>

          <Route path="/Modal">
              <Modal/>
          </Route>

        </Switch>
        
        <Footer/>
        
       




      </Router>

     

    </div>
  );
}

export default App;
